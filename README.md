# 7 Day Incidence of COVID-19 in Germany

An interactive chart of 7 day COVID-19 incidence in Germany, using data from a
[spreadsheet](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Fallzahlen.html)
maintained by the Robert Koch Institut.

It's updated daily and visibe [here](https://chrisberkhout.gitlab.io/7-day-incidence/).
