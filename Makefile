help: ## This help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

setup: ## Install dependencies
	pipenv --version || pip install --user pipenv
	pipenv install

run: ## Run notebook
	pipenv run jupyter lab

html: ## Generate HTML chart
	pipenv run jupyter nbconvert --to python --stdout 7DayIncidence.ipynb | pipenv run python

download: ## Download a copy of the current spreadsheet
	curl -o `date -u +%FT%TZ`_Fallzahlen_Kum_Tab_aktuell.xlsx 'https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Fallzahlen_Kum_Tab_aktuell.xlsx?__blob=publicationFile'
	curl -o `date -u +%FT%TZ`_Fallzahlen_Kum_Tab_Archiv.xlsx 'https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Daten/Fallzahlen_Kum_Tab_Archiv.xlsx?__blob=publicationFile'
